# Naveksoft license management library
---

## Usage: Initialize license key format
---
```cpp
using field_t = naveksoft::clicense::field_t;
naveksoft::clicense license{ "secret-key",{
	{field_t::ltype,2}, 				// predefined field <license-type>
	{field_t::string,2,"MODE"}, 		// custom string field with width 2 symbol
	{field_t::number,4,"NUM-HOSTS"}, 	// custom number field
	{field_t::lexpire}, 				// predefinied license expire field
	{field_t::lcounter,6}, 				// predefinied license number field
} };
```

## Usage: Load public key and validate license
---
```cpp
license.ImportPublicKey("/home/irokez/projects/license-tester/public.key");
license.ImportLicenseString("msOF007001404CFF000006896487CF2B83D7E5168A6B11E5B6AB6C276464BB78579CC64AB9032AB13AEB5128B0FAE34DB81768F928801FC5D060ECEE164EAF00ED7C6E6067148E0EFDB538");

/* Get hardware id */
naveksoft::chardwareid hid;
auto h_mac = hid.MAC();
auto h_cpu = hid.CPU();
auto h_os = hid.OS();
auto h_hw = hid.HARDWARE();
auto hwId = hid.Id(h_mac, h_cpu, h_os, h_hw);

bool LicenseIsValid, KeyIsValid, LicenseIsExpire, HardwareIsValid;
size_t HardwareToleranceErrors;
bool IsValid = license.Validate(LicenseIsValid, KeyIsValid, LicenseIsExpire, HardwareIsValid, hwId, HardwareToleranceErrors);
```

## Usage: Get license attributes
---
```cpp
std::tm tm;
license.GetLicExpire().timestamp(tm);
auto mode = license["MODE"];
auto nh = license["NUM-HOSTS"].number();
auto nlicNumber = license.GetLicNumber().number();
```

## Usage: Generate license request
---
```cpp
license.GenerateConsoleRequest("Enter order ID", 6, hid.Id(h_mac, h_cpu, h_os, h_hw),"20.07-release");
```