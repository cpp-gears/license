#pragma once
#include <cinttypes>
#include <cstdlib>
namespace naveksoft {
	class chardwareid {
	public:
		uint16_t MAC(const char* eth = nullptr);
		uint16_t CPU();
		uint16_t OS();
		uint16_t HARDWARE();
		inline uint64_t Id(uint16_t mac = 0xFFFF, uint16_t cpu = 0xFFFF, uint16_t os = 0xFFFF, uint16_t hw = 0xFFFF) {
			uint64_t  id{ 0xffffffffffffffff };
			uint16_t* id16{ (uint16_t*)&id };
			id16[0] &= mac; id16[1] &= cpu; id16[2] &= os; id16[3] &= hw;
			return id;
		}
		inline void Get(uint64_t id, uint16_t& mac, uint16_t& cpu, uint16_t& os, uint16_t& hw) {
			uint16_t* id16{ (uint16_t*)&id };
			mac = id16[0]; cpu = id16[1]; os = id16[2]; hw = id16[3];
		}
		inline size_t Compare(uint64_t id, uint16_t mac = 0xFFFF, uint16_t cpu = 0xFFFF, uint16_t os = 0xFFFF, uint16_t hw = 0xFFFF) {
			uint16_t* id16{ (uint16_t*)&id };
			return (id16[0] != mac) + (id16[1] != cpu) + (id16[2] != os) + (id16[3] != hw);
		}
	};
}