#include "chardwareid.h"
#include <unistd.h>
#include <cstring>
#include <cerrno>
#include <sys/types.h>
#include <cstdio>
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <netpacket/packet.h>
#include <net/ethernet.h>
#include <sys/utsname.h>

using namespace naveksoft;

namespace detail {
	ssize_t readline(const char* filename, char* content, size_t max) {
		memset(content, 0, max);

		if (auto fp{ fopen(filename,"rt") }; fp) {
			auto readed = fread(content, 1, max, fp);
			fclose(fp);
			return readed;
		}
		return -errno;
	}
}

uint16_t chardwareid::MAC(const char* eth) {
	uint16_t hash{ 0xc4ce };

	struct ifaddrs* addrs{ nullptr };

	getifaddrs(&addrs);

	if (eth) {
		for (auto it{ addrs }; it; it = it->ifa_next)
		{
			if (it->ifa_addr && it->ifa_addr->sa_family == AF_PACKET && it->ifa_addr->sa_family != AF_LOCAL && strncasecmp(eth, it->ifa_name, strlen(it->ifa_name)) == 0) {
				auto adr = (uint16_t*)((struct sockaddr_ll*)(it)->ifa_addr)->sll_addr;
				hash ^= adr[0] ^ adr[1] ^ adr[2];
				break;
			}
		}
	}
	else {
		for (auto it{ addrs }; it; it = it->ifa_next)
		{
			if (it->ifa_addr && it->ifa_addr->sa_family == AF_PACKET && it->ifa_addr->sa_family != AF_LOCAL) {
				auto adr = (uint16_t*)((struct sockaddr_ll*)(it)->ifa_addr)->sll_addr;
				hash ^= adr[0] ^ adr[1] ^ adr[2];
			}
		}
	}

	freeifaddrs(addrs);
	return hash;
}

uint16_t chardwareid::CPU() {
	uint16_t hash{ 0xb9fe };
	uint16_t id[16];
	{
		unsigned eax = 1, ebx = 0, ecx = 0, edx = 0;
		unsigned eax1 = 0, ebx1 = 0, ecx1 = 0, edx1 = 0;
		__asm__("cpuid\n\t"					\
			: "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx)	\
			: "0" (1));
		__asm__("cpuid\n\t"					\
			: "=a" (eax1), "=b" (ebx1), "=c" (ecx1), "=d" (edx1)	\
			: "0" (0x80000001));
		uint32_t* info{ (uint32_t*)id };
		info[0] = eax; info[1] = 0; info[2] = ecx; info[3] = edx;
		info[4] = eax1; info[5] = ebx1; info[6] = ecx1; info[7] = edx1;
	}
	hash ^= id[0] ^ id[1] ^ id[2] ^ id[3] ^ id[4] ^ id[5] ^ id[6] ^ id[7] ^ id[8] ^ id[9] ^ id[10] ^ id[11] ^ id[12] ^ id[13] ^ id[14] ^ id[15];
	return hash;
}

uint16_t chardwareid::OS() {
	uint16_t hash{ 0x1a85 };
	struct utsname info;
	if (uname(&info) == 0) {
		for (size_t i = 0, num = (strlen(info.sysname) + 1) >> 1; i < num; i++) {
			hash ^= ((uint16_t*)(info.sysname))[i];
		}
		for (size_t i = 0, num = (strlen(info.release) + 1) >> 1; i < num; i++) {
			hash ^= ((uint16_t*)(info.release))[i];
		}
		for (size_t i = 0, num = (strlen(info.machine) + 1) >> 1; i < num; i++) {
			hash ^= ((uint16_t*)(info.machine))[i];
		}
	}
	return hash;
}

uint16_t chardwareid::HARDWARE() {
	uint16_t hash{ 0xec53 };

	uint16_t buffer[1024];
	if (detail::readline("/sys/class/dmi/id/product_serial", (char*)buffer, sizeof(buffer)) > 0) {
		for (auto i = 0; i < 1024 && buffer[i]; i++) {
			hash ^= buffer[i];
		}
	}
	if (detail::readline("/sys/class/dmi/id/product_uuid", (char*)buffer, sizeof(buffer)) > 0) {
		for (auto i = 0; i < 1024 && buffer[i]; i++) {
			hash ^= buffer[i];
		}
	}
	if (detail::readline("/sys/class/dmi/id/board_serial", (char*)buffer, sizeof(buffer)) > 0) {
		for (auto i = 0; i < 1024 && buffer[i]; i++) {
			hash ^= buffer[i];
		}
	}
	if (detail::readline("/sys/class/dmi/id/board_vendor", (char*)buffer, sizeof(buffer)) > 0) {
		for (auto i = 0; i < 1024 && buffer[i]; i++) {
			hash ^= buffer[i];
		}
	}
	return hash;
}