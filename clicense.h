#pragma once
#include <string>
#include <string_view>
#include <vector>
#include <unordered_map>
#include <ctime>

namespace naveksoft {
	class clicense {
	public:
		enum class field_t { nil = 0, number, string, hexnumber, ymd, lexpire, ltype, lcounter, lorder, lhardwareid };
		struct field {
			field() = default;
			~field() = default;
			field(field_t fmt, size_t wdth = { 0 }, const std::string& nm = {}, size_t idx = 0) : format{ fmt }, name{ nm }, width{ wdth }, index{ idx }{; }
		private:
			friend class clicense;

			field_t				format{ field_t::nil };
			std::string			name{};
			size_t				width{ 0 };
			size_t				index{ 0 };
			std::string_view	value{};
		};
		struct value_t {
			field_t format() { return fformat; }
			const std::string_view value() { return fvalue; }

			int64_t number(int radix = 10);
			std::time_t timestamp(std::tm& tm);
			std::vector<uint8_t> bin();

			std::string str() { return { fvalue.begin(),fvalue.end() }; }

			inline operator bool() { return fformat != field_t::nil && !fvalue.empty(); }
			inline bool operator == (const std::string_view& val) { return fvalue.compare(val) == 0; }
			inline bool operator != (const std::string_view& val) { return fvalue.compare(val) != 0; }
		private:
			friend class clicense;
			value_t() = default;
			value_t(const field& f) : fformat{ f.format }, fvalue{ f.value }{; }
			field_t				fformat{ field_t::nil };
			std::string_view	fvalue{};
		};
		clicense() = delete;
		clicense(const clicense&) = delete;
		clicense(const clicense&&) = delete;
		clicense& operator = (const clicense&) = delete;
		clicense& operator = (const clicense&&) = delete;
		clicense(const std::string& licenseSecret, const std::initializer_list<clicense::field>& licenseFormat = {});
		~clicense();
		ssize_t ImportPublicKey(const std::string& pubkeyFile);
		ssize_t ImportLicenseFile(const std::string& licenseFile, const std::initializer_list<clicense::field>& licenseFormat = {});
		ssize_t ImportLicenseJson(const std::string& licenseFile, const std::initializer_list<clicense::field>& licenseFormat = {});
		ssize_t ImportLicenseString(const std::string& licenseString, const std::initializer_list<clicense::field>& licenseFormat = {});

		inline clicense::value_t GetLicType() { return fldType != -1 ? licFieldsList[fldType] : clicense::value_t{}; }
		inline clicense::value_t GetLicNumber() { return fldCounter != -1 ? licFieldsList[fldCounter] : clicense::value_t{}; }
		inline clicense::value_t GetLicOrder() { return fldOrder != -1 ? licFieldsList[fldOrder] : clicense::value_t{}; }
		inline clicense::value_t GetLicExpire() { return fldExpire != -1 ? licFieldsList[fldExpire] : clicense::value_t{}; }
		inline clicense::value_t GetLicHardwareId() { return fldHardwareId != -1 ? licFieldsList[fldHardwareId] : clicense::value_t{}; }

		inline clicense::value_t operator [](const std::string& fname) {
			if (auto it{ licFieldNames.find(fname) }; it != licFieldNames.end()) {
				return licFieldsList[it->second];
			}
			return clicense::value_t{};
		}

		bool Validate(bool& LicenseIsValid, bool& KeyIsValid, bool& LicenseIsExpire, bool& HardwareIsValid, uint64_t HardwareId = 0, size_t HardwareTolerance = 1);

		std::string GenerateRequest(const std::string& type, const std::string& order, const uint64_t hwid);

		void GenerateConsoleRequest(const char* banner, size_t orderlength, const uint64_t hwid, const char* version);

	private:
		enum class verify_t : uint16_t { nil = 0, lic = 0x1, key = 0x2, sign = 0x10, date = 0x20, hwid = 0x40 };
		void initFormat(const std::initializer_list<clicense::field>& licenseFormat);
		std::string			licSecretKey, licPublicKey, licLicense;
		std::string_view	licSignData, licSignParams;
		ssize_t				fldExpire{ -1 }, fldType{ -1 }, fldCounter{ -1 }, fldOrder{ -1 }, fldHardwareId{ -1 };
		std::vector<field>	licFieldsList;
		std::unordered_map<std::string, size_t>	licFieldNames;
		uint16_t			licState{ (uint16_t)verify_t::nil };
	};
}