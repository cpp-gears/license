#include "clicense.h"
#include <cstdio>
#include "cvt.h"
#include <cstring>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/md5.h>
#include <openssl/engine.h>

using namespace naveksoft;

namespace helper {
	ssize_t readfile(const std::string& filename, std::string& content) {
		if (auto fp = fopen(filename.c_str(), "rt"); fp) {
			fseek(fp, 0, SEEK_END);
			if (size_t nlength = ftell(fp); nlength > 0) {
				fseek(fp, 0, SEEK_SET);
				content.resize(nlength);
				if (fread(content.data(), 1, nlength, fp) == nlength) {
					fclose(fp);
					return 0;
				}
			}
			fclose(fp);
		}
		content.clear();
		return -errno;
	}
	uint64_t dec(const std::string_view& val) {
		uint64_t multipler{ 1 }, result{ 0 };
		for (auto it = val.rbegin(); it != val.rend(); ++it, multipler *= 10) {
			result += multipler * cvt::dec(*it);
		}
		return result;
	}
	uint64_t hex(const std::string_view& val) {
		uint64_t multipler{ 1 }, result{ 0 };
		for (auto it = val.rbegin(); it != val.rend(); ++it, multipler *= 16) {
			result += multipler * cvt::hex(*it);
		}
		return result;
	}
	std::vector<uint8_t> bin(const std::string_view& val) {
		std::vector<uint8_t> result;
		result.resize((val.size() + 1) >> 1, 0);
		size_t nbyte{ 0 };
		for (auto it = val.begin(); it != val.end(); ++it, ++nbyte) {
			result[nbyte >> 1ul] += (uint8_t)(cvt::hex(*it) << size_t(4ul * (1ul - (nbyte & 1ul))));
		}
		return result;
	}
	uint64_t yyyymmdd(std::time_t ts) {
		std::tm tm;
		localtime_r(&ts, &tm);
		return tm.tm_mday + (tm.tm_mon + 1) * 100 + (tm.tm_year + 1900) * 10000;
	}
}

int64_t clicense::value_t::number(int radix) {
	return (int64_t)(radix == 16 ? helper::hex(fvalue) : helper::dec(fvalue));
}

std::time_t clicense::value_t::timestamp(std::tm& tm) {
	auto yyyymmdd = helper::hex(fvalue);
	memset(&tm, 0, sizeof(tm));
	tm.tm_mday = (int)(yyyymmdd % 100u);
	tm.tm_mon = (int)((((yyyymmdd - tm.tm_mday) / 100) % 100) - 1);
	tm.tm_year = (int)(((yyyymmdd - (yyyymmdd % 10000)) / 10000) - 1900);
	tm.tm_hour = 23;
	tm.tm_min = 59;
	tm.tm_sec = 59;
	tm.tm_wday = 0;
	tm.tm_yday = 0;
	auto tms = mktime(&tm);
	localtime_r(&tms, &tm);
	return tms;
}

bool clicense::Validate(bool& LicenseIsValid, bool& KeyIsValid, bool& LicenseIsExpire, bool& HardwareIsValid, uint64_t HardwareId, size_t HardwareTolerance) {

	LicenseIsValid = KeyIsValid = LicenseIsExpire = HardwareIsValid = false;

	licState = 0;

	if (!licSignData.empty() && !licSignParams.empty()) {
		if ((licState & (uint16_t)verify_t::lic) == 0) {
			licState = (uint16_t)verify_t::lic;
			if (BIO* bufpub{ BIO_new_mem_buf((void*)licPublicKey.data(), (int)licPublicKey.size()) }; bufpub) {
				if (RSA* rsaPublic{ PEM_read_bio_RSA_PUBKEY(bufpub, nullptr, 0, nullptr) }; rsaPublic) {
					licState |= (uint16_t)verify_t::key;

					std::string data{ licSignParams.begin(),licSignParams.end() };
					data.append(licSecretKey);
					auto&& sign = helper::bin(licSignData);
					unsigned char hash[MD5_DIGEST_LENGTH];
					MD5((unsigned char*)data.data(), data.length(), hash);

					licState |= (uint16_t)(RSA_verify(NID_md5, hash, MD5_DIGEST_LENGTH, sign.data(), (unsigned)sign.size(), rsaPublic) == 1 ? ((uint16_t)verify_t::sign) : (uint16_t)(0 & 0xf));
					RSA_free(rsaPublic);
				}
				BIO_free(bufpub);
			}
		}
	}
	KeyIsValid = (licState & ((uint16_t)verify_t::lic | (uint16_t)verify_t::key)) == ((uint16_t)verify_t::lic | (uint16_t)verify_t::key);
	LicenseIsValid = KeyIsValid && (licState & (uint16_t)verify_t::sign);
	LicenseIsExpire = false;

	if (auto&& expire = GetLicExpire(); expire) {
		if (auto date{ helper::hex(expire.fvalue) }; date) {
			LicenseIsExpire = helper::yyyymmdd(std::time(nullptr)) > date;
		}
	}
	if (HardwareId) {
		if (fldHardwareId) {
			if (uint64_t LicHwId{ (uint64_t)GetLicHardwareId().number(16) }; !(HardwareIsValid = HardwareId == LicHwId)) {
				/* check num changes */
				uint16_t* u16Src{ (uint16_t*)&LicHwId }, * u16Dst{ (uint16_t*)&HardwareId };
				size_t nchanges{ size_t(u16Src[0] != u16Dst[0]) + size_t(u16Src[1] != u16Dst[1]) + size_t(u16Src[2] != u16Dst[2]) + size_t(u16Src[3] != u16Dst[3]) };
				HardwareIsValid = nchanges <= HardwareTolerance;
			}
		}
	}
	else if (!fldHardwareId) {
		HardwareId = true;
	}

	return LicenseIsValid && (LicenseIsExpire == false) && HardwareIsValid;
}

void clicense::GenerateConsoleRequest(const char* banner,size_t orderlength, const uint64_t hwid,const char* version) {
	std::string text{ banner };
	std::string format("%2[A-Z]%" + std::to_string(orderlength) + "[A-Za-z0-9]");

	text = "[ " + std::to_string(2 + orderlength) + " symbols ]";
	printf("%-15s %20s : ", banner, text.c_str());

	auto strType{ (char*)alloca(3) }, strOrder{ (char*)alloca(orderlength + 1) }; 
	int vMajor{ 0 }, vMinor{ 0 };

	if (version) {
		sscanf(version, "%4d%*c%4d", &vMajor, &vMinor);
	}

	if (auto result = scanf(format.c_str(), strType, strOrder); result == 2 && std::strlen(strOrder) == orderlength) {
		auto licRequest{ GenerateRequest(strType,strOrder,hwid) };
		printf("%-15s %20s : %s%s%04X%04X%s\n", "Request ID","[ copy this value ]", strType, strOrder, vMajor, vMinor, licRequest.c_str());
	}
	else {
		printf("Invalid Order Id\nExit.\n");
	}
	exit(0);
}

std::string clicense::GenerateRequest(const std::string& type, const std::string& order, const uint64_t hwid) {
	std::string lic_request{ "Invalid license request" };
	if (BIO* bufpub{ BIO_new_mem_buf((void*)licPublicKey.data(), (int)licPublicKey.size()) }; bufpub) {
		if (RSA* rsaPublic{ PEM_read_bio_RSA_PUBKEY(bufpub, nullptr, 0, nullptr) }; rsaPublic) {
			char hwstr[32]; snprintf(hwstr, sizeof(hwstr), "%016llX", hwid);
			std::string lic_data{ type + "#" + hwstr + "#" + order + "#" + licSecretKey };

			int rsaLen = RSA_size(rsaPublic);
			unsigned char* ed = (unsigned char*)malloc(rsaLen);
			memset(ed, 0, rsaLen);

			if (auto size = RSA_public_encrypt((int)lic_data.size(), (const unsigned char*)lic_data.data(), ed, rsaPublic, RSA_PKCS1_PADDING); size != -1) {

				BIO* bio, * b64;
				BUF_MEM* bufferPtr;

				b64 = BIO_new(BIO_f_base64());
				bio = BIO_new(BIO_s_mem());
				bio = BIO_push(b64, bio);

				BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL); //Ignore newlines - write everything in one line
				BIO_write(bio, ed, size);
				BIO_flush(bio);
				BIO_get_mem_ptr(bio, &bufferPtr);
				lic_request.assign((*bufferPtr).data, (*bufferPtr).data + (*bufferPtr).length);
				BIO_free_all(bio);
			}
			free(ed);
			RSA_free(rsaPublic);
		}
		BIO_free(bufpub);
	}
	return lic_request;
}

std::vector<uint8_t> clicense::value_t::bin() {
	return helper::bin(fvalue);
}

clicense::clicense(const std::string& licenseSecret, const std::initializer_list<clicense::field>& licenseFormat) :
	licSecretKey{ licenseSecret }
{
	initFormat(licenseFormat);
}

clicense::~clicense() {

}

void clicense::initFormat(const std::initializer_list<clicense::field>& licenseFormat) {
	size_t nIndex{ 0 };

	licFieldsList.clear();
	fldExpire = fldType = fldCounter = fldOrder = fldHardwareId = -1;
	licFieldsList.reserve(licenseFormat.size());

	for (auto&& fld : licenseFormat) {
		licFieldsList.emplace_back(fld.format, fld.width, fld.name, nIndex);

		if (fld.format == field_t::lexpire) { fldExpire = nIndex; licFieldsList[nIndex].width = 8; }
		else if (fld.format == field_t::ltype) { fldType = nIndex; }
		else if (fld.format == field_t::lcounter) { fldCounter = nIndex; }
		else if (fld.format == field_t::lorder) { fldOrder = nIndex; }
		else if (fld.format == field_t::lhardwareid) { fldHardwareId = nIndex; }
		else if (fld.format == field_t::ymd) { licFieldsList[nIndex].width = 8; }
		else {
			licFieldNames.emplace(fld.name, nIndex);
		}
		++nIndex;
	}
}

ssize_t clicense::ImportPublicKey(const std::string& pubkeyFile) {
	return helper::readfile(pubkeyFile, licPublicKey);
}

ssize_t clicense::ImportLicenseJson(const std::string& licenseFile, const std::initializer_list<clicense::field>& licenseFormat) {
	return 0;
}

ssize_t clicense::ImportLicenseFile(const std::string& licenseFile, const std::initializer_list<clicense::field>& licenseFormat) {
	if (std::string licenseString; helper::readfile(licenseFile, licenseString) == 0) {
		return ImportLicenseString(licenseString, licenseFormat);
	}
	return -EINVAL;
}


ssize_t clicense::ImportLicenseString(const std::string& licenseString, const std::initializer_list<clicense::field>& licenseFormat) {

	if (licenseFormat.size()) { initFormat(licenseFormat); }
	licState = 0;
	licLicense.assign(licenseString);

	if (!licenseString.empty()) {
		size_t npos{ 0 };
		std::string_view src{ licLicense.data(),licLicense.length() };
		bool bIsValid{ false };
		for (size_t i = 0; i < licFieldsList.size() && (bIsValid = (npos + licFieldsList[i].width) < licLicense.length()); npos += licFieldsList[i].width, i++) {
			licFieldsList[i].value = src.substr(npos, licFieldsList[i].width);
		}
		if (bIsValid) {
			licSignData = src.substr(npos);
			licSignParams = src.substr(0, npos);
			return !licSignData.empty() ? 0 : -EINVAL;
		}
	}

	return -EINVAL;
}
