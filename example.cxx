#include <cstdio>
#include "clicense.h"
#include <unordered_set>
#include "chardwareid.h"

void print_table(std::vector<std::pair<char, char>> symb) {
    std::unordered_set<uint8_t> listof;
    for (auto [f, t] : symb) {
        for (; f <= t; f++) {
            listof.emplace(f);
        }
    }
    for (int i = 0; i < 256; i++) {
        (i % 16) == 0 && printf("\n");
        printf("%c, ", listof.find((uint8_t)i) == listof.end() ? '0' : (uint8_t)i);
    }
}

int main()
{
    //print_table({ {'0','9'},{'a','z'},{'A','Z'} });

    using field_t = naveksoft::clicense::field_t;
    naveksoft::clicense license{ "license-key",{
        {field_t::ltype,2},{field_t::string,2,"MODE"},{field_t::number,4,"NUM-HOSTS"},{field_t::lexpire},{field_t::lcounter,6},

    } };

    naveksoft::chardwareid hid;
    auto h_mac = hid.MAC();
    auto h_cpu = hid.CPU();
    auto h_os = hid.OS();
    auto h_hw = hid.HARDWARE();
    auto id = hid.Id(h_mac, h_cpu, h_os, h_hw);
    auto nfails = hid.Compare(id, 4, h_cpu, h_os, h_hw);

    license.ImportPublicKey("/home/irokez/projects/license-tester/public.key");
    license.ImportLicenseString("msOF007001404CFF000006896487CF2B83D7E5168A6B11E5B6AB6C276464BB78579CC64AB9032AB13AEB5128B0FAE34DB81768F928801FC5D060ECEE164EAF00ED7C6E6067148E0EFDB538");

    bool b1, b2,b3,b4;
    uint64_t hwid{ 0 };
    size_t hwerror{ 0 };

    auto val = license.Validate(b1, b2,b3,b4, id,hwerror);

    std::tm tm;

    license.GetLicExpire().timestamp(tm);

    auto mode = license["MODE"];
    auto nh = license["NUM-HOSTS"].number();
    auto nlin = license.GetLicNumber().number();

    bool bm = mode == "MS";
    
    license.GenerateConsoleRequest("Enter order ID", 6, hid.Id(h_mac, h_cpu, h_os, h_hw),"20.07-release");

    /*printf("Enter order ID: [8 symbols]: ");
    
    char Type[64];
    char OrderId[64];

    scanf("%2[A-Za-z]%[A-Z0-9a-z]",Type, OrderId);

    auto request = license.GenerateRequest(license.GetLicType().str(), license["MODE"].str(), hid.Id(h_mac, h_cpu, h_os, h_hw));
    */
    return 0;
}